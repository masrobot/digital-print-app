package com.example.masrobot.adtadigital.model

import com.google.firebase.firestore.IgnoreExtraProperties

@IgnoreExtraProperties
class UserProfiles {
    var uid: String? = null
    var nama: String? = null
    var telepon: String? = null
    var alamatJalan: String? = null
    var alamatDesa: String? = null
    var alamatKecamatan: String? = null
    var alamatKabupaten: String? = null
    var email: String? = null
    var createAt: String? = null
    var updateAt: String? = null
}