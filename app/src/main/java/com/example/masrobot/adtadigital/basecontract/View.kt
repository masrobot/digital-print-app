package com.example.masrobot.adtadigital.basecontract

interface View {
    fun onAttachView()
    fun onDetachView()
}