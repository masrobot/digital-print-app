package com.example.masrobot.adtadigital.utils

import android.annotation.SuppressLint
import android.view.View
import java.text.DateFormat.getDateTimeInstance

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

@SuppressLint("SimpleDateFormat")
fun timestampFormat(date: Long?): String? {
    val dateFormat = getDateTimeInstance()
    val timestampToDateFormat = dateFormat.format(date)
    val result = timestampToDateFormat
    return result
}