package com.example.masrobot.adtadigital.basecontract

interface Presenter<T: View> {
    fun onAttach(view: T)
    fun onDetach()
}