package com.example.masrobot.adtadigital.model

import com.google.firebase.firestore.IgnoreExtraProperties

@IgnoreExtraProperties
class Kategori {
    var uid: String? = null
    var namaKategori: String? = null
    var imgKategori: String? = null
}