package com.example.masrobot.adtadigital.adduserprofile

import com.example.masrobot.adtadigital.basecontract.Presenter
import com.example.masrobot.adtadigital.utils.timestampFormat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.sql.Timestamp

class AddUserProfilePresenter: Presenter<AddUserProfileView> {

    private var mView: AddUserProfileView? = null
    private lateinit var mDatabase: FirebaseFirestore
    private lateinit var mAuth: FirebaseAuth

    override fun onAttach(view: AddUserProfileView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun firebaseInit() {
        mDatabase = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
    }

    fun fireGetCurrentUserAuth(): Boolean = mAuth.currentUser != null

    fun teleponData() {
        mView?.textTelepon(mAuth.currentUser?.phoneNumber.toString())
    }

    private fun validateForm(nama: String?, email: String?, telepon: String?, alamat: String?,
                             desa: String?, kecamatan: String?, kabupaten: String?): Boolean {
        var validation = true

        when {
            nama?.isEmpty() as Boolean -> {
                validation = false
                mView?.errorNama("Nama Lengkap harus diisi")
            }
        }

        when {
            email?.isEmpty() as Boolean -> {
                validation = false
                mView?.errorEmail("Email harus diisi")
            }
            email.isNotEmpty() && !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                validation = false
                mView?.errorEmail("Format email salah")
            }
        }

        when {
            telepon?.isEmpty() as Boolean -> {
                validation = false
                mView?.errorTelepon("Telepon harus diisi")
            }
        }

        when {
            alamat?.isEmpty() as Boolean -> {
                validation = false
                mView?.errorAlamat("Alamat Lengkap harus diisi")
            }
        }

        when {
            desa?.isEmpty() as Boolean -> {
                validation = false
                mView?.errorDesa("Desa harus diisi")
            }
        }

        when {
            kecamatan?.isEmpty() as Boolean -> {
                validation = false
                mView?.errorKecamatan("Kecamatan harus diisi")
            }
        }

        when {
            kabupaten?.isEmpty() as Boolean -> {
                validation = false
                mView?.errorKabupaten("Kabupaten harus diisi")
            }
        }

        return validation
    }

    fun saveProfile(nama: String?, email: String?, telepon: String?, alamat: String?,
                    desa: String?, kecamatan: String?, kabupaten: String?) {
        if (validateForm(nama, email, telepon, alamat, desa, kecamatan, kabupaten)) {

            val dataProfile = mapOf(
                    "uid" to mAuth.currentUser?.uid,
                    "nama" to nama,
                    "email" to email,
                    "telepon" to telepon,
                    "alamat" to alamat,
                    "desa" to desa,
                    "kecamatan" to kecamatan,
                    "kabupaten" to kabupaten,
                    "createAt" to timestampFormat(mAuth.currentUser?.metadata?.creationTimestamp),
                    "updatedAt" to "${Timestamp(System.currentTimeMillis())}"
            )
            if (fireGetCurrentUserAuth()) {
                mDatabase.collection("userProfiles").document("${mAuth.currentUser?.uid}").set(dataProfile)

                mView?.simpanData()
            } else {
                mView?.signInActivity()
            }
        }
    }
}