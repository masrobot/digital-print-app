package com.example.masrobot.adtadigital.fragments.dashboard

import com.example.masrobot.adtadigital.basecontract.View
import com.example.masrobot.adtadigital.model.Kategori

interface DashboardView: View {
    fun showLoading()
    fun hideLoading()
    fun signInActivity()
    fun addUserProfileActivity()
    fun kategoriData(kategori: List<Kategori>)
}