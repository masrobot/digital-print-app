package com.example.masrobot.adtadigital.adduserprofile

import com.example.masrobot.adtadigital.basecontract.View

interface AddUserProfileView: View {
    fun errorNama(message: String)
    fun errorEmail(message: String)
    fun errorTelepon(message: String)
    fun errorAlamat(message: String)
    fun errorDesa(message: String)
    fun errorKecamatan(message: String)
    fun errorKabupaten(message: String)
    fun simpanData()
    fun textTelepon(data: String)
    fun signInActivity()
}