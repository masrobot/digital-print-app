package com.example.masrobot.adtadigital.signin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.masrobot.adtadigital.R
import com.example.masrobot.adtadigital.main.MainActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import java.util.*

class SignInActivity : AppCompatActivity() {

    private val rc_signin = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        btn_signin.onClick {
            startActivityForResult(
                    // Get an instance of AuthUI based on the default app
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    AuthUI.IdpConfig.PhoneBuilder()
                                            .setDefaultCountryIso("id")
                                            .build()))
                            .build(),
                    rc_signin)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == rc_signin) {
            val response = IdpResponse.fromResultIntent(data)

            // Successfully signed in
            if (resultCode == Activity.RESULT_OK) {
                startActivity<MainActivity>()
                this.finish()
                //Crashlytics.log(5, TAG, "Sign In Success")
                Log.d("SignIn", "Sign In Success")
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    //Crashlytics.log(5, TAG, "Sign In Cancelled")
                    Log.d("SignIn", "Sign In Cancelled")
                }

                if (response?.error?.errorCode == ErrorCodes.NO_NETWORK) {
                    //Crashlytics.log(5, TAG, "No Internet Access")
                    Log.d("SignIn", "No Internet Access")
                }
                //Crashlytics.log(5, TAG, "Sign-in error: ${response?.error?.message} || ${response?.error}")
                Log.d("SignIn", "Sign-in error: ${response?.error?.message} || ${response?.error}")
            }
        }
    }

}
