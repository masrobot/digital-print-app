package com.example.masrobot.adtadigital.adduserprofile

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.masrobot.adtadigital.R
import com.example.masrobot.adtadigital.main.MainActivity
import com.example.masrobot.adtadigital.signin.SignInActivity
import kotlinx.android.synthetic.main.activity_add_user_profile.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class AddUserProfileActivity : AppCompatActivity(), AddUserProfileView {

    private lateinit var presenter: AddUserProfilePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user_profile)

        initPresenter()
        onAttachView()
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.firebaseInit()
        presenter.teleponData()

        supportActionBar?.title = getString(R.string.masukkan_data_diri)

        btn_simpan_data_profile.onClick {
            val nama = et_nama_lengkap?.text.toString()
            val email = et_email?.text.toString()
            val telepon = et_telepon?.text.toString()
            val alamat = et_alamat_lengkap?.text.toString()
            val desa = et_desa?.text.toString()
            val kecamatan = et_kecamatan?.text.toString()
            val kabupaten = et_kabupaten?.text.toString()
            presenter.saveProfile(nama, email, telepon, alamat, desa, kecamatan, kabupaten)
        }
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        onDetachView()
    }

    private fun initPresenter() {
        presenter = AddUserProfilePresenter()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        longToast(getString(R.string.lengkapi_data_diri))
    }

    override fun errorNama(message: String) {
        et_nama_lengkap.error = message
    }

    override fun errorEmail(message: String) {
        et_email.error = message
    }

    override fun errorTelepon(message: String) {
        et_telepon.error = message
    }

    override fun errorAlamat(message: String) {
        et_alamat_lengkap.error = message
    }

    override fun errorDesa(message: String) {
        et_desa.error = message
    }

    override fun errorKecamatan(message: String) {
        et_kecamatan.error = message
    }

    override fun errorKabupaten(message: String) {
        et_kabupaten.error = message
    }

    override fun simpanData() {
        toast(getString(R.string.profile_tersimpan))
        finish()
        startActivity<MainActivity>()
    }

    override fun signInActivity() {
        finish()
        startActivity<SignInActivity>()
    }

    override fun textTelepon(data: String) {
        et_telepon.setText(data)
    }
}
