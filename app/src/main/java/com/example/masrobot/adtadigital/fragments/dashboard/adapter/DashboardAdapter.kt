package com.example.masrobot.adtadigital.fragments.dashboard.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.adtadigital.R
import com.example.masrobot.adtadigital.model.Kategori
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick

class DashboardAdapter(private val dataKategori: List<Kategori>,
                       private val listener: (Kategori) -> Unit) : RecyclerView.Adapter<KategoriViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KategoriViewHolder =
            KategoriViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_dashboard, parent, false))

    override fun getItemCount(): Int = dataKategori.size

    override fun onBindViewHolder(holder: KategoriViewHolder, position: Int) {
        holder.bindItem(dataKategori[position], listener)
    }
}

class KategoriViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private var namaKategori = view.find<TextView>(R.id.tv_nama_kategori)
    private var imgKategori = view.find<ImageView>(R.id.img_kategori)
    private var item = view.find<CardView>(R.id.item_dashboard)

    fun bindItem(dataKategori: Kategori, listener: (Kategori) -> Unit) {
        namaKategori.text = dataKategori.namaKategori
        Picasso.with(itemView.context)
                .load(dataKategori.imgKategori)
                .resize(640, 480)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .into(imgKategori)
        item.onClick {
            listener(dataKategori)
        }
    }
}
