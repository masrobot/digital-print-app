package com.example.masrobot.adtadigital.fragments.dashboard


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.masrobot.adtadigital.R
import com.example.masrobot.adtadigital.adduserprofile.AddUserProfileActivity
import com.example.masrobot.adtadigital.fragments.dashboard.adapter.DashboardAdapter
import com.example.masrobot.adtadigital.model.Kategori
import com.example.masrobot.adtadigital.signin.SignInActivity
import com.example.masrobot.adtadigital.utils.invisible
import com.example.masrobot.adtadigital.utils.visible
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class DashboardFragment : Fragment(), DashboardView {

    private lateinit var presenter: DashboardPresenter
    private val kategoriData: MutableList<Kategori> = mutableListOf()
    private lateinit var adapter: DashboardAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initPresenter()
        onAttachView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.firebaseInit()

        adapter = DashboardAdapter(kategoriData) {
            toast("${it.namaKategori}")
        }
        recyclerview_dashboard.layoutManager = LinearLayoutManager(ctx)
        recyclerview_dashboard.adapter = adapter
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        onDetachView()
    }

    override fun onStart() {
        super.onStart()
        presenter.loadKategori()
    }

    private fun initPresenter() {
        presenter = DashboardPresenter()
    }

    override fun showLoading() {
        progress_bar_dashboard.visible()
    }

    override fun hideLoading() {
        progress_bar_dashboard.invisible()
    }

    override fun signInActivity() {
        activity?.finish()
        startActivity<SignInActivity>()
    }

    override fun addUserProfileActivity() {
        activity?.finish()
        startActivity<AddUserProfileActivity>()
    }

    override fun kategoriData(kategori: List<Kategori>) {
        kategoriData.clear()
        kategoriData.addAll(kategori)
        adapter.notifyDataSetChanged()
    }
}
