package com.example.masrobot.adtadigital.fragments.profile

import com.example.masrobot.adtadigital.basecontract.Presenter

class ProfilePresenter: Presenter<ProfileView> {

    private var mView: ProfileView? = null

    override fun onAttach(view: ProfileView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }
}