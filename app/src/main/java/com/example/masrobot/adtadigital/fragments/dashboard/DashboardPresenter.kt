package com.example.masrobot.adtadigital.fragments.dashboard

import android.util.Log
import com.example.masrobot.adtadigital.basecontract.Presenter
import com.example.masrobot.adtadigital.model.Kategori
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot

class DashboardPresenter: Presenter<DashboardView> {

    private var mView: DashboardView? = null
    private lateinit var mDatabase: FirebaseFirestore
    private lateinit var mAuth: FirebaseAuth

    override fun onAttach(view: DashboardView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun firebaseInit() {
        mDatabase = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
    }

    fun fireGetCurrentUserAuth(): Boolean = mAuth.currentUser != null

    private fun checkProfile() {
        mDatabase.collection("userProfiles")
                .document("${mAuth.currentUser?.uid}")
                .addSnapshotListener{
                    snapshot: DocumentSnapshot?, e: FirebaseFirestoreException? ->
                    if (e != null) Log.w("DashboardPresenter", "Listen Failed $e")
                    if (snapshot?.data == null) mView?.addUserProfileActivity()
                    Log.d("DashboardPresenter", snapshot?.data.toString())
                }
    }

    fun loadKategori() {
        if (fireGetCurrentUserAuth()) {
            mDatabase.collection("kategori").addSnapshotListener{
                snapshot: QuerySnapshot?, e: FirebaseFirestoreException? ->
                if (e != null) Log.w("DashboardPresenter", "Listen Failed $e")

                val data = snapshot?.toObjects(Kategori::class.java)
                mView?.showLoading()
                mView?.kategoriData(data as List<Kategori>)
                mView?.hideLoading()
            }

            // Check User Profile Data
            checkProfile()
        } else {
            mView?.signInActivity()
        }
    }
}